import module.InvoiceItemClass;

public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItemClass item1 = new InvoiceItemClass("AJK", "Paracetamol", 10, 100);

        InvoiceItemClass item2 = new InvoiceItemClass("AZC", "Lemon drink", 2, 20);
        System.out.println("Cost item 1: " + item1.getTotal());
        System.out.println("Cost item 2: " + item2.getTotal());
    }
}
